# Docs y Manuales: 
#   https://github.com/polkascan/py-substrate-interface
#   https://www.mongodb.com/docs/manual/tutorial/install-mongodb-enterprise-on-windows/#run-the-mongodb-installer
#   https://www.w3schools.com/python/python_mongodb_getstarted.asp

from substrateinterface import *
import json
import os
import pymongo


########################################################################
#   Params
########################################################################

params = json.load(open("params.json"))

URL = params["URL"]
SS58_FORMAT = params["SS58_FORMAT"]
TYPE_REGISTRY_PRESET = params["TYPE_REGISTRY_PRESET"]
CLIENT = params["CLIENT"]
NOMBRE_DB = params["NOMBRE_DB"]
NOMBRE_COLLECCION = params["NOMBRE_COLLECCION"]


########################################################################
#   Funciones
########################################################################

def jsonificar(objeto):
    """
    Usamos esta funcion para:
    -   Transformar el objeto devuelto por el api substrate-interface en un json. 
        Habian items de tipo DigestItem que no se podian guardar en mongodb, para hacerlo los transformo a json.
    -   Asegurarnos de guardar los ints como strings para guardarlos en mongodb. 
        Habian hexadecimales muy grandes que no se podian guardar porque mongodb solo puede guardar ints de 8 bytes.
    """
    tipo = type(objeto)
    if tipo.__name__ == 'dict':
        objeto_jsonificado = {}
        for key, value in objeto.items():
            objeto_jsonificado[key] = jsonificar(value)
    elif tipo.__name__ == 'list':
        objeto_jsonificado = []
        for value in objeto:
            objeto_jsonificado.append(jsonificar(value))
    else:
        try:
            objeto_jsonificado = jsonificar(objeto.value)    
        except:
            objeto_jsonificado = str(objeto)
    return objeto_jsonificado


def generar_bloque(substrate, block_hash=None, block_id=None):
    """
    Generamos un diccionario con la informacion relevante del bloque:
    {   '_id': int, 
        'bloque: {  'extrinsics' dict(['extrinsic_hash', 'extrinsic_length', 'call']), 
                    'header': dict(['digest', 'extrinsicsRoot', 'number', 'parentHash', 'stateRoot', 'hash'])},
        'events': [dict(['phase', 'extrinsic_idx', 'event', 'event_index', 'module_id', 'event_id', 'attributes', 'topics'])]
    }
    Donde:
    -   _id es el numero del bloque
    -   bloque es el el resultado de get_block que contiene informacion de extrinsics
    -   events es el resultadode get_events y contiene la informacion de eventos
    """
    if not block_hash:
        block_hash = substrate.get_block_hash(block_id=block_id)
    bloque = jsonificar(substrate.get_block(block_hash=block_hash))
    events = jsonificar(substrate.get_events(block_hash=block_hash))
    return {'_id': int(block_id), 'bloque': bloque, 'events': events}

def generar_numeros_de_bloques_para_procesar(block_number, mycol):
    """
    Generamos los numeros de los bloques posteriores al ultimo bloque guardado en mongodb y
    menores o iguales al numero de bloque block_number.
    """
    numeros_de_bloques_procesados = sorted([int(bloque['_id']) for bloque in mycol.find()])
    numeros_de_bloques_para_procesar = []
    if numeros_de_bloques_procesados:
        ultimo_bloque = numeros_de_bloques_procesados[-1]
        bloque_a_procesar = ultimo_bloque + 1
        while bloque_a_procesar < block_number:
            numeros_de_bloques_para_procesar.append(bloque_a_procesar)
            bloque_a_procesar += 1
    numeros_de_bloques_para_procesar.append(block_number)
    return numeros_de_bloques_para_procesar

def subscription_handler_export(substrate, mycol):
    """
    Devuelve una funcion que procesa cada bloque de la subscripcion, genera los datos relevantes y los guarda en la
    colleccion mycol de la base de mongodb.
    """
    def _subscription_handler_export(obj, update_nr, subscription_id, update_nr_limit=float("inf")):
        print(f"New block #{obj['header']['number']} produced by {obj['author']} | update_nr: {update_nr}")
        block_number = obj['header']['number']
        numeros_de_bloques_para_procesar = generar_numeros_de_bloques_para_procesar(block_number, mycol)
        for numero_de_bloque in numeros_de_bloques_para_procesar:
            print(' Procesando bloque {}'.format(numero_de_bloque))
            bloque = generar_bloque(substrate, block_id=numero_de_bloque)
            mycol.update_one({"_id": numero_de_bloque}, {"$set": bloque}, upsert=True)
        if update_nr > update_nr_limit:
            return {'message': 'Subscription will cancel when a value is returned', 'updates_processed': update_nr}
    return _subscription_handler_export



def generar_lista_de_id_procesados(mycol):
    """
    Genera la lista de numeros de bloques ya procesados.
    """
    return sorted([int(bloque['_id']) for bloque in mycol.find()])


def procesar_padres(substrate, mycol, block_hash=None, block_id=None, limite_de_padres=float("inf")):
    """
    A partir del hash o el id de un bloque, procesa sus bloques ancestros de forma recursiva.
    """
    if not block_id:
        block_id = substrate.get_block_number(block_hash=block_hash)
    padre_nr = 0
    while padre_nr < limite_de_padres:
        print(' Procesando bloque {} | padre_nr: {}'.format(block_id, padre_nr))
        bloque = generar_bloque(substrate, block_id=block_id)
        mycol.update_one({"_id": block_id}, {"$set": bloque}, upsert=True)
        block_id = substrate.get_block_number(block_hash=bloque['bloque']['header']['parentHash'])
        padre_nr += 1

def test():
    """
    Algunas pruebas que hice a mano como referencia.
    """
    # Test 0: Entrar a la base e insertar un diccionario
    # myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    # mydb = myclient["coinfabrik_glitch"]
    # mycol = mydb["bloques"]
    # mydict = { "_id": 111, "name": "Peter", "address": "Lowstreet 27" }
    # x = mycol.insert_one(mydict)

    # Test 1 [anduvo!] Obtener bloques
    # URL = "ws://13.212.197.46:9944"
    # substrate = SubstrateInterface( url=URL,
    #                                 ss58_format=42,
    #                                 type_registry_preset='substrate-node-template')
    # block_hash = "0x0c7b25c89024b6c0dfccc76e074b4adb1b483a7b14785abe3daeae8446d112b8"
    # result = substrate.get_block(block_hash=block_hash)

    # Test 2: Obtener eventos
    # URL = "ws://13.212.197.46:9944"
    # substrate = SubstrateInterface( url=URL,
    #                                 ss58_format=42,
    #                                 type_registry_preset='substrate-node-template')
    # block_hash = "0x6f19dd30bde20e7643bcba14521c7ef1799df5f388588335582f7be74954585e"
    # events = substrate.get_events(block_hash=block_hash)

    # Test 3: Obtener la infor de extrinsics de bloques de distintas maneras
    # URL = "ws://13.212.197.46:9944"
    # substrate = SubstrateInterface( url=URL,
    #                                 ss58_format=42,
    #                                 type_registry_preset='substrate-node-template')
    # block_hash = "0x6f19dd30bde20e7643bcba14521c7ef1799df5f388588335582f7be74954585e"
    # bloque = generar_bloque(substrate, block_hash=block_hash)
    # bloque = generar_bloque(substrate, block_id=168901)

    # Test 4: Subscribirse y obtener info de ultimos bloques.
    # substrate = SubstrateInterface( url=URL,
    #                                 ss58_format=42,
    #                                 type_registry_preset='substrate-node-template')
    # result = substrate.subscribe_block_headers(subscription_handler_export(substrate), include_author=True)




if __name__ == '__main__':
    print('#################################')
    print('#    substrateinterface_export')
    print('#################################')
    print('Params:')
    for key, value in params.items():
        print(' {}: {}'.format(key, value))
    print()
    print('Menu:')
    print(' [1] Subscribir y procesar nuevos bloques')
    print(' [2] Procesar los padres de un bloque')
    opcion = int(input('Ingrese el numero de la opcion deseada:'))
    print()
    if opcion == 1:
        print('[Opcion 1] Subscribir y procesar nuevos bloques')
        print()
        substrate = SubstrateInterface( url=URL,
                                        ss58_format=SS58_FORMAT,
                                        type_registry_preset=TYPE_REGISTRY_PRESET)
        myclient = pymongo.MongoClient("mongodb://localhost:27017/")
        mydb = myclient["coinfabrik_glitch"]
        mycol = mydb["bloques"]
        substrate.subscribe_block_headers(subscription_handler_export(substrate, mycol), include_author=True)
    elif opcion == 2:
        print('[Opcion 2] Procesar padres')
        block_id = int(input('Ingresar el block_number/block_id:'))
        print()
        substrate = SubstrateInterface( url=URL,
                                    ss58_format=SS58_FORMAT,
                                    type_registry_preset=TYPE_REGISTRY_PRESET)
        myclient = pymongo.MongoClient("mongodb://localhost:27017/")
        mydb = myclient["coinfabrik_glitch"]
        mycol = mydb["bloques"]
        procesar_padres(substrate, mycol, block_id=block_id)
    else:
        print('Opcion incorrecta, correr de vuelta el programa.')
