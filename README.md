# block_indexer

Con este script:
- Bajamos informacion de bloques, extrinsics y eventos usando el api susbtrate-interface
- Guardamos esta informacion como JSON en mongodb

## Referencias

https://github.com/polkascan/py-substrate-interface

https://www.mongodb.com/docs/manual/tutorial/install-mongodb-enterprise-on-windows/#run-the-mongodb-installer

https://www.w3schools.com/python/python_mongodb_getstarted.asp

## Setup

Para correr este script es necesario:

1. Instalar el modulo substrate-interface (https://github.com/polkascan/py-substrate-interface)
2. Instalar mongodb (https://www.mongodb.com/docs/manual/tutorial/install-mongodb-enterprise-on-windows/#run-the-mongodb-installer)
3. Instalar pymongo (python -m pip install pymongo)
4. Instalar json (pip install json)

## Interfaz

En el archivo params.json se puede parametrizar a que red y base de datos se conecta el script. \
El script cuenta con un menu de inicio para determinar el tipo de busqueda que se quiere realizar: \
[1] Procesar y subscribirse a bloques nuevos \
[2] Procesar, a partir de un bloque inicial sus bloque padres.

En la carpeta Params guardamos ejemplos de params.json. 

## Base de Datos

Se genera una base de mongodb donde, para cada bloque, guardamos el siguiente diccionario adentro de un documento 'bloques' de la coleccion 'coinfabrik_glitch':

    {   '_id': int, 
        'bloque': { 'extrinsics' dict(['extrinsic_hash', 'extrinsic_length', 'call']), 
                    'header': dict(['digest', 'extrinsicsRoot', 'number', 'parentHash', 'stateRoot', 'hash'])},
        'events': [dict(['phase', 'extrinsic_idx', 'event', 'event_index', 'module_id', 'event_id', 'attributes', 'topics'])]
    }

    Donde:
    -   _id es el numero/id del bloque
    -   bloque es el el resultado de substrate.get_block que contiene informacion de extrinsics (https://polkascan.github.io/py-substrate-interface/base.html#substrateinterface.base.SubstrateInterface.get_block)
    -   events es el resultado de substrate.get_events y contiene la informacion de eventos (https://polkascan.github.io/py-substrate-interface/base.html#substrateinterface.base.SubstrateInterface.get_events)

Observaciones:
- Los diccionarios de extrinsics y events son el resultado de transformar un objeto scalecodec que no se podia guardar en mongodb.
- Transforme todos los ints, salvo el id del bloque, en str porque algunos excedian los 8 bytes que puede guardar mongodb.

Consumos de Disco:
- Crear una base de datos en mongodb pesa de entrada 300 mb. 
- Con una prueba al 09/06/2022 guarde 3476 bloques que pesaron en total ~ 12 mb adicionales -> 3.45 kb por bloque. 
- Con el tamaño actual de los bloques estimo ~ 50 mb de disco por dia \
(60x60x24 sec por dia / 6 sec por bloque = 14400 bloques por dia, 50 mb ~ = 14400 * 3.45 kb )

## Siguientes Pasos

Dejar corriendo este script en un servidor del cliente (Glitch) para tener registro eventos.